from scrapy import signals
from scrapy.exporters import JsonLinesItemExporter
import dropbox, os
from datetime import datetime

tokens = dict(
    vbo='3GYVZ3tsONAAAAAAAAAAIUgsAEnIpNf1m4pkflItzz-cluYTOY-xXvIrYgn_Rre0',
    makelaarsland='3GYVZ3tsONAAAAAAAAAAIrmc8PHKjBwg2kxvsDaBzsvlAts49YgfvYq1zurb1Yb4',
    garantiemakelaars='3GYVZ3tsONAAAAAAAAAAIL1fkl2qsinxpU0lmLaMn-0aHY47U01GTFjNmMuc0GW4',
    pararius='3GYVZ3tsONAAAAAAAAAAL7FYvvvLVRlMUJXVaM1hCpFSMYXEcKaL0hPtNFC1a18p',
    funda_agents='3GYVZ3tsONAAAAAAAAAAVFt_xG19U6I9PSEKVHJpprxC7yifkyP8zka108S6dXVy',
    vbo_agents='3GYVZ3tsONAAAAAAAAAAVUjfoiznld6b0HDnIKpgt3eUWMeO28Uuxq2dY6DOpctA',
    era_nl='3GYVZ3tsONAAAAAAAAAAVigwMgNGtuVhYA_xi2ULAmOFfNURlIySzQeEqr4TooFM',
    jaap_nl='3GYVZ3tsONAAAAAAAAAAHxYA1SD4BEzOU6T1K1l4NGIFteztJfT0JYb-7cEbQG7A')


class NederlandRealestatePipeline(object):
    def __init__(self):
        self.files = {}

    @classmethod
    def from_crawler(cls, crawler):
        pipeline = cls()
        crawler.signals.connect(pipeline.spider_opened, signals.spider_opened)
        crawler.signals.connect(pipeline.spider_closed, signals.spider_closed)
        return pipeline

    def spider_opened(self, spider):
        file_name = '/root/nederland_realestate/%s_items.json' % spider.name
        file = open(file_name, 'w+b')
        self.files[spider] = file
        self.exporter = JsonLinesItemExporter(file)
        self.exporter.start_exporting()

    def spider_closed(self, spider):
        self.exporter.finish_exporting()
        file = self.files.pop(spider)
        file.close()
        print('Writing file done.. \nUploading to dropbox "{}" folder...'.format(spider.name))

        file_name = '/root/nederland_realestate/%s_items.json' % spider.name
        now_str = datetime.now().strftime('%d.%m.%y-%H:%M:%S')
        compressed_filename = '{}_{}.gz'.format(file_name, now_str)
        file_compressing_result = os.system('gzip -n -4 {}'.format(file_name))
        print('Compressing result:', file_compressing_result)
        os.rename(file_name + '.gz', compressed_filename)
        # if not file_compressing_result:
        #     os.remove(file_name)

        access_token = tokens[spider.name]
        mode = dropbox.files.WriteMode.overwrite
        dbx = dropbox.Dropbox(access_token)
        dropbox_name = compressed_filename.split('/')[-1]

        with open(compressed_filename, 'rb') as f:
            print(dbx.files_upload(f.read(), '/{}'.format(dropbox_name), mode, mute=False))

        os.remove(compressed_filename)
        print('Scraping done. Waiting for new iteration...')

    def process_item(self, item, spider):
        self.exporter.export_item(item)
        return item
