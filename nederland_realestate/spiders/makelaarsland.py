# -*- coding: utf-8 -*-
import scrapy


class MakelaarslandSpider(scrapy.Spider):
    name = 'makelaarsland'
    allowed_domains = ['makelaarsland.nl']
    start_urls = ['https://www.makelaarsland.nl/huis-zoeken/?_q=']

    def parse(self, response):
        pagination = [response.urljoin(x) for x in response.xpath('//a[@class="page-numbers"]/@href').extract()]
        for url in pagination:
            yield scrapy.Request(url=url, callback=self.parse)

        items = [response.urljoin(x) for x in response.xpath('//div[@class="m-search-result"]/a/@href').extract()]
        for url in items:
            yield scrapy.Request(url=url, callback=self.parse_item)

    def parse_item(self, response):
        item = {}
        item['Url'] = response.url
        item['Address'] = response.xpath('//h1[@class="m-house-detail-data__address"]/text()').extract_first().replace('  ', ', ')
        item['Desctiption'] = '\n'.join([x.strip() for x in response.xpath('//div[@id="houseDetailDescription"]//text()').extract() if x.strip()])
        item['Images'] = response.xpath('//li/@data-img-url').extract()
        item['Price'] = response.xpath('//h3[@class="m-house-detail-data__price"]/text()').extract_first().strip()

        # Getting kenmerken dictionary.
        kenmerken = {}
        sections = response.xpath('//section[@class="m-house-detail-specifications__section"]')
        for section in sections:
            section_title = section.xpath('./h2/text()').extract_first()
            kenmerken[section_title] = {}
            section_rows = section.xpath('./div[@class="m-house-detail-specifications__row"]')
            for row in section_rows:
                key = row.xpath('./h3/text()').extract_first()
                value = row.xpath('./h4/text()').extract_first()
                kenmerken[section_title][key] = value

        item['Kenmerken'] = kenmerken
        yield item
