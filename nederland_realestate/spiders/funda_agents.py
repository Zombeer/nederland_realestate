# -*- coding: utf-8 -*-
import scrapy, json, re


class FundaAgentsSpider(scrapy.Spider):
    name = "funda_agents"
    allowed_domains = ["funda.nl"]
    start_urls = ['http://www.funda.nl/makelaars/heel-nederland/']

    def start_requests(self):
        url = 'http://www.funda.nl/distil_identify_cookie.html?d_ref=/&qs='
        cookies = {
            'INLB': '01-003',
            'D_SID': '178.140.1.27:O58ILw63OhI5IBQUnQ+kQTVd+hd0tC3OrPQzaEzNU6w',
            'D_PID': 'A55AE5D3-CDE6-3B3A-ADD2-C0EC341AD7AF',
            'D_IID': '15DB9931-1BFC-339F-B54D-1FF821A96F9F',
            'D_UID': 'B978C00B-A986-372B-8E03-6ED516D620D1',
            'D_HID': 'gzL1hg3g4qw6XywzQO1DrHukeFzG7QvSve0bBCZYxXM',
            'D_ZID': '9C3D9B13-E329-3966-9C3D-D2382A9456A5',
            'D_ZUID': 'F9B2312F-6320-387A-84BD-91E406D33D0A',
            'SNLB': '12-002',
        }

        headers = {
            'DNT': '1',
            'Accept-Encoding': 'gzip, deflate, sdch',
            'Accept-Language': 'en-US,en;q=0.8,ru;q=0.6,fi;q=0.4',
            'Upgrade-Insecure-Requests': '1',
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36',
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
            'Referer': 'http://www.funda.nl/',
            'Connection': 'keep-alive',
        }
        req = scrapy.Request(url=url, callback=self.parse, cookies=cookies, headers=headers)
        return [req]

    def parse(self, response):
        if 'makelaars' not in response.url:
            yield scrapy.Request(url='http://www.funda.nl/makelaars/heel-nederland/', callback=self.parse)
            return
            
        page_count = int(response.xpath('//div[@class="pagination-numbers"]/a/text()').extract()[-1].strip())

        for i in range(1, page_count):
            formdata = {
                'filter_location': 'Land Nederland',
                'filter_location-previous': 'Land Nederland',
                'sort': 'sorteer-relevantie_Ascending',
                'pagination-page-number-next': str(i)
            }

            referer = 'http://www.funda.nl/makelaars/heel-nederland/p{}/'.format(i)
            print(referer)
            yield scrapy.FormRequest(
                url='http://www.funda.nl/makelaars/heel-nederland/{}/?ajax=true'.format(i),
                method='POST',
                dont_filter=True,
                headers={'Referer': referer},
                formdata=formdata,
                callback=self.parse_json)

    def parse_json(self, response):
        jsonresponse = json.loads(response.body_as_unicode())
        html = jsonresponse['content']['results']
        links = [response.urljoin(x) for x in re.findall('<a class="link-primary makelaars-result-item-title" href="(.+?)"', html)]
        for url in links:
            yield scrapy.Request(url=url, callback=self.parse_item)

    def parse_item(self, response):
        '''
        +kantoornaam
        +adres
        +office location (lat-long)
        +emailaddres
        +phone number
        +opening hours
        +'over ons'
        +'wat we doen'
        +url to the office page on funda
        +url of real estate agent website
        ?type of real-estate agent (aankoop, verkoop, ...)
        '''
        item = {}
        item['url'] = response.url
        item['kontoornaam'] = response.xpath('//h1[@itemprop="name"]/text()').extract_first()
        item['adres'] = ' '.join([x.strip() for x in response.xpath('//span[@class="icon icon-location"]/following-sibling::div/text()').extract()])
        item['location'] = {}
        item['location']['latitude'] = response.xpath('//meta[@itemprop="latitude"]/@content').extract_first()
        item['location']['longitude'] = response.xpath('//meta[@itemprop="longitude"]/@content').extract_first()
        item['emailaddres'] = response.xpath('//body//text()').re('[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+\w')
        item['over_ons'] = '\n'.join([
            x.replace('\xa0', '').strip() for x in response.xpath('//div[@itemprop="description"]//text()').extract() if x.replace('\xa0', '').strip()
        ])
        item['wat_we_doen'] = {}
        what_we_doin = response.xpath('//dl[@class="makelaars-features-content"]')
        for el in response.xpath('//dl[@class="makelaars-features-content"]/dt'):
            item['wat_we_doen'][el.xpath('./text()').extract_first()] = el.xpath('./following-sibling::dd/text()').extract_first()

        phone_num = response.xpath('//a[@class="object-contact-kantoor-button"]/@href').extract_first()
        if phone_num:
            item['phone_number'] = phone_num[4:]

        item['website'] = response.xpath('//a[@class="makelaars-contact-website"]/@href').extract_first()
        item['opening_hours'] = {}

        for el in response.xpath('//div[@class="makelaars-contact-item makelaars-opentimes-wrapper"]//td[@class="makelaars-contact-opentimes-day"]'):
            item['opening_hours'][el.xpath('./span[@class="fullname"]/text()').extract_first()] = ''.join(
                [x.strip() for x in el.xpath('./following-sibling::td/text()').extract() if x.strip()])

        yield item
