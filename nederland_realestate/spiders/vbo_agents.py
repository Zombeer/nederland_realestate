# -*- coding: utf-8 -*-
import scrapy


class VboAgentsSpider(scrapy.Spider):
    name = "vbo_agents"
    allowed_domains = ["vbo.nl"]
    start_urls = ['https://www.vbo.nl/makelaars-taxateurs/soort-woon.html']

    def parse(self, response):
        pages = [response.urljoin(x) for x in response.xpath('//div[@class="pagination"]//a/@href').extract()]

        for url in pages:
            yield scrapy.Request(url=url, callback=self.parse)

        items = [response.urljoin(x) for x in response.xpath('//div[@class="makelaar-list"]/a/@href').extract()]
        for url in items:
            yield scrapy.Request(url=url, callback=self.parse_item)

    def parse_item(self, response):

        item = {}
        item['url'] = response.url
        item['kantoornaam'] = response.xpath('//h1/text()').extract_first()
        contact_data = response.xpath('//address//text()').extract()

        for text in contact_data:
            if 'Tel:' in text:
                item['phone_number'] = text[4:]
            if 'Fax:' in text:
                item['fax'] = text[4:]
            if '@' in text:
                item['emailaddres'] = text
            if 'www' in text:
                item['website'] = text

        item['adres'] = ', '.join(contact_data[1:3])
        item['diensten'] = response.xpath('//li[contains(text(), "Diensten")]/em/text()').extract_first().split(', ')
        item['opening_hours'] = {}
        for tr in response.xpath('//div[@class="hidden-xs makelaar-openingstijden"]//tr'):
            td_content = tr.xpath('./td/text()').extract()
            item['opening_hours'][td_content[0]] = td_content[1]
        yield item