# -*- coding: utf-8 -*-
import scrapy, re


class JaapNlSpider(scrapy.Spider):
    name = "jaap_nl"
    allowed_domains = ["jaap.nl"]
    start_urls = ['http://www.jaap.nl/koophuizen/p1', 'http://www.jaap.nl/huurhuizen/p1']

    def parse(self, response):
        pagination_href = response.xpath('//a[@class="navigation-button "]/@href').extract()[-1].split('/')
        page_count = pagination_href[-1][1:]
        
        if response.url.endswith('p1'):
            for i in range(int(page_count)):
                url = '/'.join(pagination_href[:-1]) + '/p' + str(i)
                url=response.urljoin(url)
                yield scrapy.Request(url=url, callback=self.parse)
                
        items = [x.split('?')[0] for x in response.xpath('//a[@class="property-inner"]/@href').extract()]
        for url in items:
            yield scrapy.Request(url=url, callback=self.parse_item)
        
        
    def parse_item(self, response):
        url = response.url
        if 'te-koop' in url:
            offer_type = 'Sell'
        else:
            offer_type = 'Rent'
            
        title = response.xpath('//title/text()').extract_first()
        status = 'Open'
        if title.startswith('Verhuurd'):
            status = 'Rented out'
        elif title.startswith('Verkocht'):
            status = 'Sold out'
            
        try:
            street_str = response.xpath('//div[@class="detail-address-street"]/text()').extract_first()
        except:
            return
        try:
            zip_str = response.xpath('//div[@class="detail-address-zipcity"]/text()').extract_first()
            city = zip_str[8:]
            zip_str = zip_str[:8]
        except:
            try:
                zip_str = re.findall('(\d{4} [A-Z]{2})', street_str)[0]
                city = re.findall('\d{4} [A-Z]{2} (.+)', street_str)[0]
                street_str = street_str.replace(zip_str, '').replace(city, '').replace(',', '').strip()
            except:
                zip_str = ''
                city = re.findall(', (.+)', street_str)[0]
                street_str = street_str.replace(city, '').replace(',', '').strip()

                print('No zip found at {}'.format(response.url))
                print('Street: {} , City: {}'.format(street_str, city))
        
        price_str = response.xpath('//div[@class="detail-address-price"]/text()').extract_first().replace(' Tophuis', '')
        
        
        try:
            description = response.xpath('//div[@id="long-description"]/text()').extract_first().replace(' - Toon minder Meer kenmerken »', '')
        except:
            description = response.xpath('//div[@class="short-description"]/text()').extract_first().replace(' - Toon minder Meer kenmerken »', '')

        image_srcs = ['http:' + x for x in response.xpath('//div[@class="photo"]/div/img/@data-lazy-load-src').extract() if 'spacer.gif' not in x]

        broker_name = response.xpath('//div[@class="broker-name"]/text()').extract_first()
        try:
            broker_phone = re.findall(r'"phoneNumber": ?"(.+?)"', response.body_as_unicode())[0]
        except:
            broker_phone = ''
        
        broker = dict(Name=broker_name, Phone=broker_phone)


        # Kenmerken
        kenmerken = {}
        tables = response.xpath('//div[@class="detail-tab-content kenmerken"]/table')
        for table in tables:
            table_name = table.xpath('.//th/text()').extract_first().replace('\n','').replace('\r','').strip()
            kenmerken[table_name] = {}

            rows = table.xpath('.//tr')
            for row in rows:
                try:
                    row_key = row.xpath('.//td[@class="name"]/div[@class="no-dots"]/text()').extract_first().replace('\n','').replace('\r','').strip()
                    row_value = row.xpath('.//td[@class="value"]/text()').extract_first().strip()
                    kenmerken[table_name][row_key] = row_value
                except:
                    continue

        date_added = response.xpath('//div[@class="detail-tab-content woningwaarde"]//td[@class="value"]/text()').extract_first().replace('\n','').replace('\r','').strip()
        address = dict(City=city, Zip=zip_str, Street=street_str)

        item = {'Url': url, 'Status': status, 'Type': offer_type, 'Photos' : image_srcs, 'Description': description, 'Broker': broker, 'Kenmerken': kenmerken, 'Date_Added' : date_added, 'Address': address}
        yield item