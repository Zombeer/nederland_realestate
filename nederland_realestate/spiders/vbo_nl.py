import scrapy

scraped_links = []
# with open('vbo_previous.txt','r') as f:
#     scraped_links = f.read().split('\n')

class VbolNlSpider(scrapy.Spider):
    name = "vbo"
    allowed_domains = ["vbo.nl"]
    start_urls = (
        'https://www.vbo.nl/koopwoningen/land-nederland?p=1&l=5000',
    )

    def parse(self, response):
        next_link = response.xpath('//span[text()=">"]/parent::a/@href').extract_first()
        if next_link:
            print('Yielding new catalog page....')
            yield scrapy.Request(response.urljoin(next_link),self.parse)
            
        links = [response.urljoin(x) for x in response.xpath('//figure[@class="object-tiles"]/a/@href').extract()]
        for link in links:
            if link not in scraped_links:
                yield scrapy.Request(link, self.parse_page)
                pass
    
    def parse_page(self, response):
        url = response.url
        images = response.xpath('//div[@class="slideshow owl-carousel"]/figure/img/@src').extract()
        description = ''.join(response.xpath('//div[@data-content="omschrijving"]/div[@class="container"]/div[@class="row"]//text()').extract()[3:]).strip().replace('\n','').replace('\t','')
        address = response.xpath('//h1/text()').extract_first().strip() + ', ' + response.xpath('//h2/span[@class="hidden-xs"]/text()').extract_first().strip() + ', ' + response.xpath('//h2/span[@class="hidden-xs"]/following::text()').extract_first().strip()
        price = response.xpath('//span[@class="object-price"]/text()').extract_first()
        agent = {}
        agent['Name'] = response.xpath('//div[@class="object-makelaar"]/h2/a/text()').extract_first()
        agent_info = ''.join(response.xpath('//div[@class="object-makelaar"]/p//text()').extract()).replace('\t','').split('\n')[1:]
        agent['Address'] = agent_info[0]+', ' + agent_info[1]
        agent['Phone'] = agent_info[2].replace('T: ','')
        agent['Email'] = agent_info[3].replace('E: ','')
        agent['Website'] = agent_info[4].replace('W: ','')

        data = response.xpath('//div[@class="col-xs-12 col-md-8 object-kenmerken"]')
        table_names = data.xpath('.//h3/text()').extract()
        tables = data.xpath('.//table')
        
        kenmerken = {}
        
        for i,table in enumerate(tables):
            kenmerken[table_names[i]] = {}
            for row in table.xpath('.//tr'):
                tds = row.xpath('.//td/text()').extract()
                try:
                    if tds[1].count(', '):
                        kenmerken[table_names[i]][tds[0]] = tds[1].split(', ')
                    else:
                        kenmerken[table_names[i]][tds[0]] = tds[1]
                except:
                    pass
        
        result = dict(Url=url, Images=images, Description=description, Price=price, Address=address, Broker=agent, Kenmerken=kenmerken)
        yield result