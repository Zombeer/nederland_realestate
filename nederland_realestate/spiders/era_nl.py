# -*- coding: utf-8 -*-
import scrapy
import csv

zip_list = []
with open('nl_postal_codes.csv', 'r', encoding='cp1252') as f:
    for line in f.read().split('\n')[1:]:
        zip_list.append(line[:4])

print('---====== Total postal codes found:', len(zip_list), '======---')


class EraNlSpider(scrapy.Spider):
    name = "era_nl"
    total_results = 0
    allowed_domains = ["era.nl"]

    def start_requests(self):
        requests = []
        for postcode in zip_list[:]:

            form_data = {
                'zoektekst-type': '',
                'zoektekst': '',
                'prijsmin': '0',
                'prijsmax': '',
                'prijsmin': '0',
                'prijsmax': '$null',
                'soort': 'koop',
                'objecttype': '',
                'kamers': '',
                'postcode': postcode,
                'radius': '10',
                'woningtekst': '',
                'zoeklabel': '',
                'extrazoekopties': ''
            }

            req = scrapy.FormRequest(url='https://www.era.nl/aanmeldingen_lijst', callback=self.parse, formdata=form_data)
            req.meta['initial'] = True
            requests.append(req)
        return requests

    def parse(self, response):
        pages = [response.urljoin(x) for x in response.xpath('//div[@class="result-page-nav"]/a/@href').extract()]
        if pages and response.meta['initial']:
            page_count = int(pages[-2].split('=')[1])
            for i in range(2, page_count + 1):
                req = scrapy.Request(
                    url='https://www.era.nl/nl/aanmeldingen_lijst.vm?currentpage={}'.format(i), callback=self.parse, dont_filter=True)
                req.meta['initial'] = False
                yield req

        items = [response.urljoin(x) for x in response.xpath('//div[@class="result-tile-info-wrapper"]/a/@href').extract()]
        for url in items:
            yield scrapy.Request(url=url, callback=self.parse_item)

    def parse_item(self, response):

        item = {}
        item['url'] = response.url
        item['adres'] = response.xpath('//h3[text()="Waar staat dit huis?"]/following-sibling::p/b/text()').extract_first()
        item['prijs'] = ' '.join(response.xpath('//div[@class="object-blue-bar"]/text()').extract_first().replace('\n', '').split())
        item['foto'] = [response.urljoin(x) for x in response.xpath('//body').re('Fotoraster.add\(.+? "(.+?)"')]
        video = response.xpath('//iframe[@class="videoframe"]/@src').extract_first()
        if video:
            item['video'] = video.replace('/embed/', '/watch?v=')
        item['kenmerken'] = [x.strip().replace('\n', '') for x in response.xpath('//ul[@class="kenmerken"]/li/text()').extract()]
        item['contactpersoon'] = {}
        item['contactpersoon']['name'] = response.xpath('//div[@class="makelaarsbox-groot-kantoornaam"]/text()').extract_first().strip()
        links = response.xpath('//a[@class="makelaarsbox-groot-naam"]/@href').extract()
        for l in links:
            if 'tel:' in l:
                item['contactpersoon']['phone'] = l[4:]
            if 'mailto:' in l:
                item['contactpersoon']['email'] = l[7:]

        req = scrapy.FormRequest(
            url='https://www.era.nl/nl/vm/aanmelding_omschrijving.vm',
            method='POST',
            callback=self.parse_desc,
            formdata={'toonHeleOmschrijving': 'true'},
            dont_filter=True)
        req.meta['item'] = item
        yield req

    def parse_desc(self, response):
        item = response.meta['item']
        item['omschrijving'] = '\n'.join([x.strip() for x in response.xpath('//div[@id="aanmelding_omschrijving"]//text()').extract()])
        yield item