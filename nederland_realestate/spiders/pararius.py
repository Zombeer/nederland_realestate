import scrapy

class ParariusSpider(scrapy.Spider):
    name = "pararius"
    allowed_domains = ["pararius.nl"]
    start_urls = (
        'https://www.pararius.nl/huurwoningen/nederland',
    )

    def parse(self, response):
        items = [response.urljoin(x) for x in response.xpath('//h2/a/@href').extract()]
        for item in items:
            yield scrapy.Request(url=item, callback=self.parse_item)
            
        pages = [response.urljoin(x) for x in response.xpath('//ul[@class="pagination"]/li/a/@href').extract()]
        for page in pages:
            yield scrapy.Request(url=page, callback=self.parse)
        pass
    
    def parse_item(self, response):
        try:
            item = {}
            item['Url'] =  response.url
            item['Images'] = [x for x in response.xpath('//li[@class="centered-image-container"]/img/@src').extract() if '613x920' in x]
            item['Images'].extend([x for x in response.xpath('//li[@class="centered-image-container"]/img/@data-src').extract() if '613x920' in x])
            item['Description'] = '\n'.join(response.xpath('//div[@itemprop="description"]/p/text()').extract())
            item['Price'] = response.xpath('//meta[@itemprop="price"]/@content').extract_first()
    
            kenmerken = {}
            kenmerken['Details'] = {}
            
            details = response.xpath('//div[@id="details"]/dl')
            for dd in details.xpath('./dt'):
                title = dd.xpath('./text()').extract_first()
                value = dd.xpath('./following-sibling::dd/text()').extract_first()
                kenmerken['Details'][title] = value
            
            kenmerken['Features'] = [x.strip() for x in response.xpath('//ul[@class="features"]/li/text()').extract()]
            
            broker = {}
            broker['Website'] = response.xpath('//a[@class="cta website"]/@href').extract_first()
            broker['Phone'] = response.xpath('//a[@class="cta telephone"]/@data-telephone').extract_first()
            broker['Name'] = response.xpath('//div[@id="estate-agent"]/p/a/text()').extract_first()
            try:
                broker['Address'] = response.xpath('//div[@id="estate-agent"]/p/text()[preceding-sibling::br]').extract_first().replace('\n   ','').strip()
            except:
                broker['Address'] = ''
            
            item['Kenmerken'] = kenmerken
            item['Broker'] = broker
            item['Coordinates'] = {}
            item['Coordinates']['Latitude'] = response.xpath('//div[@class="map"]/div[@class="canvas"]/@data-lat').extract_first()
            item['Coordinates']['Longitude'] = response.xpath('//div[@class="map"]/div[@class="canvas"]/@data-lng').extract_first()
            yield item
        
        except:
            with open('errors.txt','a') as f:
                f.write(response.url+'\n')

