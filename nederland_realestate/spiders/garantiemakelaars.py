import scrapy

def deobfuscate(text):
    if text:
        text = text.lower().replace('#','@')
    else:
        return ''
    result = []
    dictionary = {'a': 'n', 'c': 'p', 'b': 'o', 'e': 'r',
    'd': 'q', 'g': 't', 'f': 's', 'i': 'v', 'h': 'u', 'k': 'x', 
    'j': 'w', 'm': 'z', 'l': 'y', 'o': 'b', 'n': 'a', 'q': 'd',
    'p': 'c', 's': 'f', 'r': 'e', 'u': 'h', 't': 'g', 'w': 'j',
    'v': 'i', 'y': 'l', 'x': 'k', 'z': 'm'}
    for letter in text:
        try:
            result.append(dictionary[letter])
        except:
            result.append(letter)
            
    return ''.join(result).strip()
        


class GarantiemakelaarsSpider(scrapy.Spider):
    name = "garantiemakelaars"
    allowed_domains = ["garantiemakelaars.nl"]
    start_urls = ('http://www.garantiemakelaars.nl/12-cff6/aanbod-resultaten?PropertyType=Residential&ForSaleOrRent=FOR_SALE&Take=5000&SearchUsingWidget=true&IsCustom=true',)
        

    def parse(self, response):
        links = [response.urljoin(x) for x in response.xpath('//a[@class="sys-property-link"]/@href').extract()]
        for link in links:
            yield scrapy.Request(link, self.parse_page)
        pass

    def parse_page(self, response):
        kenmerken = {}
        agent = {}
        agent['name'] = response.xpath('//div[@class="object_detail_department_name"]/text()').extract_first()
        agent['email'] = deobfuscate(response.xpath('//a[@class="object_detail_department_email obfuscated-mail-link"]/@data-mailto').extract_first())
        agent['phone'] = response.xpath('//div[@class="object_detail_department_phone"]/text()').extract_first()
        
        if agent['phone']:
            agent['phone'] = agent['phone'].strip()
            
        agent['website'] = response.xpath('//a[@class="object_detail_department_website"]/@href').extract_first() 
            
        address = response.xpath('//h2[@class="obj_address"]/text()').extract_first()[9:]                                          
        url = response.url
        description = ''.join(response.xpath('//div[@class="description"]/text()').extract()).strip()
        price = response.xpath('//span[@class="obj_price"]/text()').extract_first()
        images = response.xpath('//a[contains(@class,"item gallery-link")]/@href').extract()
        plans = response.xpath('//a[@data-gallery="listing-floorplans"]/@href').extract()
        video = response.xpath('//source[@type="video/mp4"]/@src').extract() 
        tables = response.xpath('//div[@id="object_features_anker"]//table')  
        
        
        for table in tables:                                                                                                                                                                                   
            header = table.xpath('.//th/text()').extract_first()                                                                                                                                                
            kenmerken[header] = {}
            for tr in table.xpath('.//tr')[1:]:
                tds = tr.xpath('.//td/text()').extract()
                if not tds[1].count(', '):
                    kenmerken[header][tds[0]] = tds[1]
                else:
                    kenmerken[header][tds[0]] = tds[1].split(', ')
                
        result = dict(Url=url, Images=images, Video=video, Price=price, Description=description, Address=address, Plans=plans, Kenmerken=kenmerken, Broker=agent)
                
        yield result
        
                
                